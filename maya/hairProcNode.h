#include <extension/Extension.h>
#include <extension/ExtensionsManager.h>
#include <translators/shape/ShapeTranslator.h>

#include <maya/MPxSurfaceShape.h>
#include <maya/MPxSurfaceShapeUI.h>
#include <maya/MDrawData.h>
#include <maya/MDrawRequest.h>
#include <maya/MString.h>
#include <maya/MDagPath.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MVector.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MColor.h>
#include <maya/M3dView.h>
#include <maya/MFnPlugin.h>
#include <maya/MDistance.h>
#include <maya/MMatrix.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MGlobal.h>

#include "cyHairFile.h"



class HairProcNode : public MPxSurfaceShape
{
public:
    HairProcNode();
    virtual ~HairProcNode();

    void postConstructor() {setRenderable(true);}

    virtual MStatus         compute( const MPlug& plug, MDataBlock& data );

    void* getGeometry();

    virtual bool            isBounded() const;
    virtual MBoundingBox    boundingBox() const;

    static  void *          creator();
    static  MStatus         initialize();

    static MObject          _aHairProcPath;
    static  MObject         _aHairFileFn;
    static MObject          _aDummy;
    static MObject          _aPreviewQuality;
    static MObject          _aMinPixelWidth;
    bool _valid;
    MPoint                  _bbmin;
    MPoint                  _bbmax;
    GLuint                  _displayList;
    cyHairFile              _hairFile;

public:
    static  MTypeId     id;
    static  MString     drawDbClassification;
    static  MString     drawRegistrantId;
};

class HairProcNodeUI : public MPxSurfaceShapeUI
{
public:
    virtual void draw(const MDrawRequest& drawRequest, M3dView& view) const;
    virtual void getDrawRequests(const MDrawInfo& info, bool objectAndActiveOnly, MDrawRequestQueue& queue);
    virtual bool    select( MSelectInfo &selectInfo,
                            MSelectionList &selectionList,
                            MPointArray &worldSpaceSelectPts ) const;
    static void* creator();
protected:
};

class HairProcNodeTranslator : public CShapeTranslator
{
public:
    static void* create();

    bool IsMayaTypeRenderable() { return true;}

protected:
    virtual void Export(AtNode* node);
    virtual void Update(AtNode* node);
    virtual AtNode* CreateArnoldNodes();
};